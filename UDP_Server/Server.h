/** Server.h
* ===========================================================
* Name: Jodi Dalton, Date
* Section: T1A
* Project: <put the assignment information here>
* Purpose: <description of purpose of the program
* Documentation: * ===========================================================
*/

//
// Created by C21Jodi.Dalton on 9/3/2019.
//

#ifndef SERVER_SERVER_H
#define SERVER_SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>

#define PORT 4240
#define MAXLINE 1024

void songList(int socket, struct sockaddr_in  clientAddress, int addrLength);
void streamSong(int socket, struct sockaddr_in clientAddress, int addrLength, char *buffer);
int getFrameSize(char c);

#endif //SERVER_SERVER_H
