//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/
#include "Server.h"

int main()
{

        int socketfd;  //The file descriptor for the socket
        struct sockaddr_in srvaddr, cliaddr; //A struct that holds the IP Address and the port number
        char buffer[MAXLINE]; //The message from the client is stored in the buffer
        char *hello = "Hello from server"; //Message sent back to the client

        // Creating The socket
        // AF_INET is the domain/IP Address
        // SOCK_DGRAM = "type" = UDP, unreliable
        // protocol=0, specifies UDP within SOCK_DGRAM type.
        if ((socketfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            perror("socket failed");
            exit(EXIT_FAILURE);
        }

        srvaddr.sin_family = AF_INET;           // Domain/IP Address
        srvaddr.sin_addr.s_addr = INADDR_ANY;   // All available interfaces
        srvaddr.sin_port = htons(
                PORT);       // port, converted to network byte order (prevents little/big endian confusion between hosts)



        // Opens the socket on port 8080
        // Bind expects a sockaddr, we created a sockaddr_in.  (struct sockaddr *) casts pointer type to sockaddr.
        if (bind(socketfd, (const struct sockaddr *) &srvaddr, sizeof(srvaddr)) < 0) {
            perror("bind failed");
            exit(EXIT_FAILURE);
        }

        printf("Waiting for connection...\n");

        while(1 == 1){
        // Receive message from client
        int len = sizeof(cliaddr), n; //length is the size of the message recieved from server
        if ((n = recvfrom(socketfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &cliaddr, &len)) < 0) {
            perror("ERROR");
            printf("Errno: %d. ", errno);
            exit(EXIT_FAILURE);
        }
        buffer[n] = '\0';  //terminate message
        printf("Client : %s\n", buffer);

        // Sends the response back to the client
        if (!strcasecmp(buffer, "LIST_REQUEST")) {
            printf("List request received!\n");
            songList(socketfd, cliaddr, len);
        } else if(!strcasecmp(buffer, "START_STREAM\nToms Diner.mp3") | (!strcasecmp(buffer,"START_STREAM\nWe Didn't Start the Fire.mp3"))){
            printf("Start Stream recieved!");
            streamSong(socketfd, cliaddr, len, buffer);
        }

    }
    //sendto(socketfd, (const char *)hello, strlen(hello), 0, (const struct sockaddr *) &cliaddr, len);
    //printf("Hello message sent.\n");
    //close(socketfd); // Close socket
    //return 0;
}

//This function sends the list of songs back to the client when "List songs" is requested
//int socket is the socket file descriptor
//struct socketaddr_in holds the IP Address and port number
//int client adddress is the IP address of the client
//Addr Length is the lenth of the message getting sent
void songList(int socket, struct sockaddr_in clientAddress, int addrLength){

    char ListOfSong[MAXLINE];

    strcpy(ListOfSong, "LIST_REPLY\n");

    char *songOne = "Suzanne Vega - Toms Diner.mp3\n";
    char *songTwo = "Billy Joel - We Didn't Start the Fire.mp3\n";

    strcat(ListOfSong, songOne);
    strcat(ListOfSong, songTwo);

    //printf("%s", ListOfSong);

    sendto(socket, (const char *)ListOfSong, strlen(ListOfSong), 0, (const struct sockaddr *) &clientAddress, addrLength);
    printf("MP3 list sent to client\n");
    printf("Waiting for connection...\n");

}

void streamSong(int socket, struct sockaddr_in clientAddress, int addrLength, char *buffer) {
    FILE *fp;

    if (!strcmp(buffer, "START_STREAM\\Toms Diner.mp3")) {
        fp = fopen("Suzanne Vega - Toms Diner.mp3", "rb");

        if (fp == NULL) {
            printf("Error creating data file in readCadetBlk: %s.\n", strerror(errno));
            exit(1);
        }

    } else if (!strcmp(buffer, "START_STREAM\nWe Didn't Start the Fire.mp3")){
        fp = fopen("Billy Joel - We Didn't Start the Fire.mp3", "rb");

        if (fp == NULL) {
            printf("Error creating data file in readCadetBlk: %s.\n", strerror(errno));
            exit(1);
        }
    }

    int frameNum = 1;

    char songFile[MAXLINE];

    while(!feof(fp)){
        char c = fgetc(fp);
        if(c == 0xFF){
            c = fgetc(fp);
            if(c == 0xFB){
                c = fgetc(fp);
                //Perfrom bitwise operations to get the frame size
                int sizeOfFrame = getFrameSize(c);

                printf("Frame %d Found! Size: %d bytes. \n", frameNum, sizeOfFrame);
                frameNum = frameNum + 1;

                //Reverse 3 bytes fseek(fp, -3, c)
                fseek(fp, -3, SEEK_CUR);


                strcpy(songFile, "STREAM_DATA\n");


                for (int i = 0; i < sizeOfFrame; i++){
                    songFile[strlen("STREAM_DATA\n") + i] = fgetc(fp);
                }

                sendto(socket, (const char *)songFile, strlen("STREAM_DATA\n") + sizeOfFrame, 0, (const struct sockaddr *) &clientAddress, addrLength);
                usleep(1);

            }else{
                fseek(fp, -1, c); //reverse back one byte
            }
        }
    }

}




int getFrameSize(char c){
    int sizeOfFrame;

    int bitLookup[] = {0,32,40,48,56,64,80,96,112,128,160,192,224,256,320,-1};
    int sampleLookup[] = {44100, 48000, 32000, -1};

    char bitRate = c;

    bitRate = c >> 4;

    int IntBitRate = (bitLookup[bitRate] * 1000);

    char sampleRate = c;

    sampleRate = sampleRate >> 2;
    sampleRate = sampleRate & 3;

    int IntSampleRate = sampleLookup[sampleRate];

    char padding = c;

    padding = padding >> 1;
    padding = padding & 1;

    sizeOfFrame = ((144 * IntBitRate / IntSampleRate) + (int)padding);

    return sizeOfFrame;

}