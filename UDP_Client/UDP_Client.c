//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/

#include "UDP_Client.h"


// Driver code
int main() {
// ...socket creation and other initialization would happen here...
    int sockfd; //Socket descriptor, like a file-handle
    char buffer[MAXLINE]; //buffer to store message from server
    char *hello = "Hello from client"; //message to send to server
    struct sockaddr_in servaddr;  //we don't bind to a socket to send UDP traffic, so we only need to configure server address

    char *request;




    while (1) {

        menuDisplay();
        int option = userInput();
        if(option==3)
        {
            break;
        }

        // Creating socket file descriptor
        if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            perror("socket creation failed");
            exit(EXIT_FAILURE);
        }

        // Filling server information
        servaddr.sin_family = AF_INET; //IPv4
        servaddr.sin_port = htons(
                PORT); // port, converted to network byte order (prevents little/big endian confusion between hosts)
        servaddr.sin_addr.s_addr = INADDR_ANY; //localhost


        // Set the timeout for the socket:
        struct timeval timeout; //structure to hold our timeout
        timeout.tv_sec = 5; //5 second timeout
        timeout.tv_usec = 0; //0 milliseconds
        if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(timeout)) < 0) {
            perror("setsockopt failed");
            exit(EXIT_FAILURE);
        }

// ...address configuration and possibly a send command would happen here...

        ssize_t n;
        int len = sizeof(servaddr);
        //printf("made it here\n");
// Receive data with a timeout:


        //Sending message to server
        //sendto(sockfd, (const char *)hello, strlen(hello), 0, (const struct sockaddr *) &servaddr, sizeof(servaddr));
        //printf("Hello message sent.\n");
        if (option == 1) {
            request = "LIST_REQUEST";
            sendto(sockfd, (const char *) request, strlen(request), 0, (const struct sockaddr *) &servaddr,
                   sizeof(servaddr));
            printf("Requesting a list of songs\n");
            printf("Songs available:\n");
        } else if (option == 2) {
            int songNum = songChoice();
            if(songNum == 1){
                request = "START_STREAM\nSuzanne Vega - Toms Diner.mp3";
            } else{
                request = "START_STREAM\nBilly Joel - We Didn't Start the Fire.mp3";
            }
            sendto(sockfd, (const char *) request, strlen(request), 0, (const struct sockaddr *) &servaddr,
                   sizeof(servaddr));
            recieveSong(sockfd, servaddr, len, buffer, n);
        } else {
            //Do nothing, let program exit
        }


        if ((n = recvfrom(sockfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &servaddr, &len)) < 0) {
            // Handle errors here.  Errno 11, "Resource Temporarily Unavailable" is returned as a result of a timeout.
            perror("ERROR");
            printf("Errno: %d. ", errno);

            if (errno == 11) {
                printf("The client timed out after 5 seconds\n");
            }
            exit(EXIT_FAILURE);
        }




// ...do something with the response...


        buffer[n] = '\0'; //terminate message
        for (int i = 0; i < strlen(buffer); i++) {
            buffer[i] = buffer[i + 11];
        }
        printf("%s\n", buffer);
    }

close(sockfd);
return 0;

}

void menuDisplay(){
    printf("Enter one of the following commands:\n");
    printf("\"1\" = List Songs\n\"2\" = Stream Songs\n\"3\" = Exit\n ");
}

int userInput(){
    int userChoice;
    scanf(" %d", &userChoice);

    if(userChoice >= 1 && userChoice <= 3){
        return userChoice;
    } else {
        printf("Invalid input, please try again\n");
        userInput();
    }
}

int songChoice(){

    int songNum = 0;
    char * userChoice;

    printf("Please enter a song name: ");

    while ((getchar()) != '\n');

    fgets( userChoice, MAXLINE, stdin);
    //scanf(" %s", userChoice);

    if(strstr(userChoice, "We Didn't Start the Fire")){
        songNum = 2;
    } else if(strstr(userChoice, "Toms Diner")){
        songNum = 1;
    } else {
        printf("That's not an option silly...try again");
        songChoice();
    }

    return songNum;
}

void recieveSong(int socket, struct sockaddr_in serverAddresss, int addrLength, char* buffer, int n){
    int numOfFrames = 0;
    int sizeOfFrames = 0;

    if((n = recvfrom(socket, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &serverAddresss, &addrLength)) < 0){
        printf("Server Unavailable");
    }

    FILE *fp = fopen("New Mp3 File.mp3","wb");

    if (fp == NULL) {
        printf("Error creating new file in receiveSong: %s.\n", strerror(errno));
        exit(1);
    }

    while(!strstr(buffer, "STREAM_DONE")){
        numOfFrames = numOfFrames + 1;
        sizeOfFrames = sizeOfFrames + (n-12);

        for(int i = 12; i < n; i++){
            fprintf(fp, "%c", buffer[i]);
        }

        printf("Frame #%d received with %d bytes\n", numOfFrames, (n-12));

        if((n = recvfrom(socket, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &serverAddresss, &addrLength)) < 0){
            printf("Server Unavailable");
        }

        buffer[n] = '\0';
    }

    printf("Stream done. Total Frames: %d. Total Size: %d bytes. \n Done!\n", numOfFrames, sizeOfFrames);
    fclose(fp);

}
