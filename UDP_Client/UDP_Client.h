/** UDP_Client.h
* ===========================================================
* Name: Jodi Dalton, Date
* Section: T1A
* Project: <put the assignment information here>
* Purpose: <description of purpose of the program
* Documentation: * ===========================================================
*/

//
// Created by C21Jodi.Dalton on 9/3/2019.
//

#ifndef UDP_CLIENT_UDP_CLIENT_H
#define UDP_CLIENT_UDP_CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/time.h>
#include <stdbool.h>
#include <sys/time.h>
#define PORT    4240
#define MAXLINE 1024

void menuDisplay();
int userInput();
int songChoice();
void recieveSong(int socket, struct sockaddr_in serverAddresss, int addrLength, char *buffer, int n);


#endif //UDP_CLIENT_UDP_CLIENT_H
